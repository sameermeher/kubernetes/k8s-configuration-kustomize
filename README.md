# K8s Configuration Kustomize
## Kubernetes Manifest files for deploying applications

## Commands to view the generated manifest file by Kustomize -
For Base -
    kubectl kustomize app-1/base/
    kustomize build app-1/base/
For Dev namespace  - 
    kubectl kustomize app-1/overlays/dev/
    kustomize build app-1/overlays/dev/
For Prod namespace - 
    kubectl kustomize app-1/overlays/prod/
    kustomize build app-1/overlays/prod/

## Commands to apply the generated manifest file by Kustomize -
For Dev namespace  - 
	kubectl apply -k app-1/overlays/dev/
	kustomize build app-1/overlays/dev/ | kubectl apply -f -
For Prod namespace - 
	kubectl apply -k app-1/overlays/prod/
	kustomize build app-1/overlays/prod/ | kubectl apply -f -
